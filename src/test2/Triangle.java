package test2;

public class Triangle extends Shape implements ThreeDimensionalShapeInterface1{
	private double base;
	private double height;
	private String color;
	public Triangle(double d, double h, String c)
	{
		this.base = d;
		this.height = h;
		this.color = c;
	}
	public double getBase() {
		return base;
	}
	public void setBase(double base) {
		this.base = base;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	@Override
	public double calculateVolume() {
		double volume = (base*height)/2;
		return volume;
		
	}
	@Override
	public double printDetails() {
     System.out.println("the volume"+this.calculateVolume());
     System.out.println("color"+this.color);
     System.out.println(this.base +"."+this.height);
     return 0.0;

	}
	
	

}

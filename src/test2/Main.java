package test2;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
	
//	Student Name = Ramanpreet Kaur
//	Student id = c0748669

	
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int choice = 0;
		ArrayList<Shape>storeShape = new ArrayList<>();
		

		while (choice != 4) {
			// 1. show the menu
			showMenu();
	
			// 2. get the user input
			System.out.println("Enter a number: ");
			choice = keyboard.nextInt();
			
			// 3. DEBUG: Output what the user typed in 
			System.out.println("You entered: " + choice);
			System.out.println();
			if(choice==1)
			{
			System.out.println("Enter the dimension of shape");
			int side = keyboard.nextInt();
			Square s1 = new Square(side, "Red");
			s1.calculateArea();
			s1.printInfo();
			storeShape.add(s1);
			
			}
			else if(choice==2)
			{
				System.out.println("Enter the dimension of shape");
				int base = keyboard.nextInt();
				int height = keyboard.nextInt();
				Triangle t1 = new Triangle(base, height,"yellow");
				t1.calculateVolume();
				t1.printDetails();
				storeShape.add(t1);
			}
			else if(choice==3)
			{
		
				System.out.println("Exit the program");
				System.out.println("The stored shapes are");
				System.out.println("Square"+storeShape);
				
			 
				
			}
			
			
		}
	}
	
	public static void showMenu() {
		System.out.println("AREA GENERATOR");
		System.out.println("==============");
		System.out.println("1. Triangle");
		System.out.println("2. Square");
		System.out.println("3. Rectangle");
		System.out.println("4. Exit");
		
		
	}

}


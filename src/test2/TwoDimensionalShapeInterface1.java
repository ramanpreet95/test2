package test2;

public interface TwoDimensionalShapeInterface1 {
	// Returns the area of a 2D Shape
			public double calculateArea();
			
			// Prints out information pertinent to a shape
			public void printInfo();

			double calculateVolume();
}

package test2;

public class Square extends Shape  implements TwoDimensionalShapeInterface1
{
	private int side;
	private String color;
	public Square(int side1, String clr)
	{
		this.side = side1;
		this.color = clr;
	}
	
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getSide() {
		return side;
	}
	public void setSide(int side) {
		this.side = side;
	}
	
	@Override
	public double calculateArea() {
	    double area = this.side*this.side;
		return area;
	}
	@Override
	public void printInfo() {
		 System.out.println("The area of the square  "+ this.calculateArea()); 
		 System.out.println("The color of the shape  "+this.color);
		 System.out.println("The dimension of the shape  "+this.side);
		
	}

	@Override
	public double calculateVolume() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	

}
